// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
	setupFiles: ['<rootDir>/jest.setup.js'],
	testEnvironment: 'jsdom',
	clearMocks: true,
};

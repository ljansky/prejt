// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import { addComponents } from './utils/components/component-factory';
import { createReduxStore } from './store';
import AppRouter from './components/router';

if (process.env.NODE_ENV !== 'production') {
	const {whyDidYouUpdate} = require('why-did-you-update');
	whyDidYouUpdate(React);
}

export const init = (config: any) => {
	const history = createBrowserHistory();
	const app = document.createElement('div');
	if (document && document.body) {
		document.body.appendChild(app);
	}

	addComponents(config.components);
	const store = createReduxStore(config.config, history);
	
	ReactDOM.render(<Provider store={store}>
		<AppRouter history={history} />
	</Provider>, app);
};

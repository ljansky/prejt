import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter as Router } from 'connected-react-router';
import Module from './module';

const AppRouter = props => (<Router history={props.history}>
	<Switch>
		<Route exact path='/' component={Module}/>
		<Route exact path='/:module' component={Module}/>
		<Route exact path='/:module/:id' component={Module}/>
	</Switch>
</Router>);

export default AppRouter;
import React from 'react';
import Endpoint from '../default/endpoint';

class BaseAutocomplete extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			search: ''
		};

		this.search = this.search.bind(this);
		this.resetSearch = this.resetSearch.bind(this);
	}

	getConfigPath (attr) {
		return `/${this.props.dataPaths.config.join('/')}/${attr}`;
	}

	getSearchField () {
		return this.props.childConfigs[0].attributes.label.substring(2);
	}

	getSelectedName () {
		const { config } = this.props.data;
		return config && config.selected && config.selected[this.getSearchField()];
	}

	search ({ target: { value } }) {
		this.setState({ search: value }, () => {
			this.props.onChange({ target: { value: this.state.search } }, this.getConfigPath('search'));
		});
	}

	resetSearch () {
		const selectedName = this.getSelectedName();
		this.setState({ search: selectedName });
	}

	componentDidUpdate (props) {
		const name = this.getSelectedName();
		const prevName = props.data.config && props.data.config.selected && props.data.config.selected[this.getSearchField()];
		
		if (name !== prevName) {
			this.setState({ search: name });
		}
	}

	render () {
		const { data: { value }, dataPaths, attributes, Wrap } = this.props;
		const { search } = this.state;

		const searchEndpointProps = {
			attributes: {
				fetch: attributes.fetch,
				store: this.getConfigPath('items')
			},
			childConfigs: [{
				name: 'query',
				attributes: {
					name: `f.${this.getSearchField()}`,
					value: `${this.getConfigPath('search')}|operator(like)`
				}
			}]
		};

		const loadEndpointProps = {
			attributes: {
				fetch: attributes.fetch,
				store: this.getConfigPath('selected')
			},
			childConfigs: [{
				name: 'param',
				attributes: {
					name: 'id',
					value: (typeof dataPaths.value === 'string') ? dataPaths.value : `/${dataPaths.value.join('/')}`
				}
			}]
		};

		const searching = this.state.search && this.state.search !== '' && this.state.search !== this.getSelectedName();

		const data = Object.assign({}, this.props.data, {
			items: this.props.data.config ? this.props.data.config.items : []
		});

		const attributesWithItems = Object.assign({}, this.props.attributes, {
			items: this.getConfigPath('items')
		});

		return (<React.Fragment>
			{searching && <Endpoint {...searchEndpointProps} dataPath={this.props.dataPath} />}
			{value && <Endpoint {...loadEndpointProps} dataPath={this.props.dataPath} />}
			<Wrap
				{...this.props}
				attributes={attributesWithItems}
				data={data}
				searching={searching}
				searchText={search}
				search={this.search}
				resetSearch={this.resetSearch}
			/>
		</React.Fragment>);
	}
}

export default {
	component: (Component) => props => (<BaseAutocomplete {...props} Wrap={Component} />),
	dataToProps: ['config']
};
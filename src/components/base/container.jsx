import React from 'react';
import { connect } from 'react-redux';
import { getComponent, getWrapProps } from '../../utils/components';
import { componentsFetch, componentsInit, componentResolve } from '../../store/components/components.actions';
import getComponentChildren from '../../store/components/components.selectors';
import { equals } from 'ramda';

const getDiffs = (a, b) => {
	const result = [];
	for (let i in a) {
		if (a[i] !== b[i]) {
			result.push(i);
		}
	}

	return result;
};

const containers = {};

function areEqualShallow(a, b, exceptions) {
	for(var key in a) {
		if(a[key] !== b[key] && !exceptions.includes(key)) {
			return false;
		}
	}
	return true;
}

class Container extends React.Component {

	componentDidMount () {
		if (this.props.data && this.props.data.module) {
			this.props.componentsFetch('modules/' + this.props.data.module + '.xml', this.props.id, this.props.templateVariables);
		} else if (this.props.childConfigs && this.props.childConfigs.length) {
			this.props.componentsInit(this.props.childConfigs, this.props.id);
		}
	}

	componentDidUpdate (prevProps) {
		if (this.props.data && this.props.data.module && this.props.data.module !== prevProps.data.module) {
			this.props.componentsFetch('modules/' + this.props.data.module + '.xml', this.props.id, this.props.templateVariables);
		} else if (!equals(this.props.childConfigs, prevProps.childConfigs)) {
			this.props.componentsInit(this.props.childConfigs, this.props.id);
		}
	}
	
	shouldComponentUpdate (nextProps) {
		if (areEqualShallow(this.props, nextProps, ['items', 'children', 'childConfigs']) && equals(this.props.items, nextProps.items) && equals(this.props.childConfigs, nextProps.childConfigs)) {
			return false;
		}
		return true;
	}

	debugInfo (debugMode = false) {
		if (!debugMode) {
			return null;
		}

		if (!containers[this.props.id]) {
			containers[this.props.id] = 0;
		}

		return ` [C-${this.props.id}(${containers[this.props.id]++})] `;
	}

	render () {
		const items = this.props.items.map(getComponent);
		const Wrap = this.props.Wrap;
		const props = getWrapProps(this.props);

		return <React.Fragment>{this.debugInfo()}{Wrap ? <Wrap {...props}>{items}</Wrap> : items}</React.Fragment>;
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		configPath: ownProps.module ? 'modules/' + ownProps.module + '.xml' : null,
		items: getComponentChildren(state, ownProps.id),
		resolved: state.components.index[ownProps.id] && state.components.index[ownProps.id].resolved
	};
};

const mapDispatchToProps = dispatch => ({
	componentsFetch: (path, id, templateVariables) => dispatch(componentsFetch(path, id, templateVariables)),
	componentsInit: (configs, id) => dispatch(componentsInit(configs, id)),
	unresolve: (id) => dispatch(componentResolve(id, false))
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);
import React from 'react';
import Base from './base';

export default {
	component: (Component) => {
		const BaseComponent = props => <Base {...props} Wrap={Component} />;
		return BaseComponent;
	},
	dataToProps: ['enabled']
};
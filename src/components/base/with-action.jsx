import { actionCall } from '../../store/actions/actions.actions';
import { splitActionString } from '../../utils/common';

export default {
	dispatchToProps: dispatch => ({
		callAction: (actionString, props) => dispatch(actionCall(splitActionString(actionString), props, null))
	})
};

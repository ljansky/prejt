import { compose, reduce, append, type, concat } from 'ramda';
import withData from './with-data';

export default (...withComponents) => Component => {
	const result = reduce((acc, curr) => {
		let components = acc.components;
		let dataToProps = acc.dataToProps;
		let dispatchToProps = acc.dispatchToProps;

		if (type(curr) === 'Function') {
			components = append(curr, components);
		} else if (type(curr) === 'Array') {
			dataToProps = concat(dataToProps, curr);
		} else {
			if (type(curr.component) === 'Function') {
				components = append(curr.component, components);
			}

			if (type(curr.dataToProps) === 'Array') {
				dataToProps = concat(dataToProps, curr.dataToProps);
			}

			if (type(curr.dispatchToProps) === 'Function') {
				dispatchToProps = append(curr.dispatchToProps, dispatchToProps);
			}
		}

		return { components, dataToProps, dispatchToProps };
	}, {
		components: [],
		dataToProps: [],
		dispatchToProps: []
	})(withComponents);

	return compose(withData(result.dataToProps, result.dispatchToProps), ...result.components)(Component);
};
import { connect } from 'react-redux';
import { reduce, merge } from 'ramda';
import { collectDataForComponent } from '../../store/data/data.selectors';

export default (mapDataToProps = [], mapDispatchToPropsArray = []) => {
	const mapStateToProps = (state, ownProps) => {
		const data = collectDataForComponent(state, ownProps, mapDataToProps);
		return {
			data: data.values,
			dataPaths: data.paths
		};
	};

	const mapDispatchToProps = dispatch => reduce((acc, curr) => {
		return merge(acc, curr(dispatch));
	}, {})(mapDispatchToPropsArray);
	
	return connect(mapStateToProps, mapDispatchToProps);
};

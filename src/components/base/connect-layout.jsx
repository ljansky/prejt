import React from 'react';
import { concat } from 'ramda';
import BaseLayout from './base-layout';
import connectBase from './connect-base';

export default (Component, mapDataToProps = []) => {
	const BaseComponent = props => <BaseLayout {...props} Wrap={Component} />;
	const dataToProps = concat(['module'], mapDataToProps);
	return connectBase(BaseComponent, dataToProps);
};
import React from 'react';
import Items from './items';

export default Item => ({
	component: Component => {
		const BaseComponent = props => {
			return (<Items {...props} Wrap={Component} Item={Item} />);
		};
		return BaseComponent;
	},
	dataToProps: ['items']
});
import React from 'react';
import BaseLayout from './base-layout';

export default {
	component: (Component) => {
		const BaseComponent = props => <BaseLayout {...props} Wrap={Component} />;
		return BaseComponent;
	},
	dataToProps: ['module']
};
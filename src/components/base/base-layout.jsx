import React from 'react';
import { getWrapProps } from '../../utils/components';
import DefaultWrap from './default-wrap';

/**
 * Base component for most of other components. It is used inside other components to wrap the content of the component.
 * It adds the functionality to set layout of component - width. Width is set in component definition in XML file of module as parameter. Its a number from 1 to 12
 * and it means the same as col-md-{number} class in bootstrap framework
 */
class BaseLayout extends React.Component {

	constructor(props) {
		super(props);
		this.getStyles = this.getStyles.bind(this);
	}

	getStyles () {
		const row = this.props.attributes.direction === 'row';

		const styles = {
			display: 'flex',
			flex: this.props.attributes.flex || '1',
			flexDirection: row ? 'row' : 'column',
			alignItems: row ? 'flex-start' : 'stretch',
			justifyContent: row ? 'space-around' : 'flex-start'
		};

		return styles;
	}

	render () {

		const props = getWrapProps(this.props, {
			getStyles: this.getStyles
		});

		const Wrap = this.props.Wrap || DefaultWrap;
		return (<Wrap {...props}>{this.props.children}</Wrap>);
	}
}

export default BaseLayout;
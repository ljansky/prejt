import React from 'react';
import { range, merge, slice, pluck } from 'ramda';
import { connect } from 'react-redux';
import { componentsInit, componentsRemove } from '../../store/components/components.actions';
import getComponentChildren from '../../store/components/components.selectors';

import { equals } from 'ramda';

const containers = {};

const getDiffs = (a, b) => {
	const result = [];
	for (let i in a) {
		if (a[i] !== b[i]) {
			result.push(i);
		}
	}

	return result;
};

function areEqualShallow(a, b, exceptions) {
	for(var key in a) {
		if(a[key] !== b[key] && !exceptions.includes(key)) {
			return false;
		}
	}
	return true;
}

export class Items extends React.Component {

	constructor(props) {
		super(props);
		this.getWrapProps = this.getWrapProps.bind(this);
	}

	componentDidMount () {
		this.refreshItems();
	}

	componentDidUpdate (props) {
		if (this.props.data.items !== props.data.items) {
			this.refreshItems();
		}
	}
	
	shouldComponentUpdate (nextProps) {
		if (areEqualShallow(this.props, nextProps, ['items', 'children']) && equals(this.props.items, nextProps.items)) {
			return false;
		}
		return true;
	}

	refreshItems () {
		if (this.props.childConfigs) {
			const dataLength = this.props.data.items ? this.props.data.items.length : 0;
			const componentsLength = this.props.items ? this.props.items.length : 0;
			const childNumberChange = dataLength - componentsLength;

			const itemConfig = this.props.childConfigs[0];
			if (childNumberChange > 0) {
				const itemConfigs = range(componentsLength, dataLength)
					.map((index) => ({
						name: 'item',
						attributes: merge(itemConfig.attributes, {
							data: this.props.attributes.items + '/' + index
						}),
						childConfigs: itemConfig.childConfigs
					}));

				this.props.componentsInit(itemConfigs, this.props.id);
			}

			if (childNumberChange < 0) {
				const itemsToRemove = slice(childNumberChange, Infinity)(this.props.items);
				this.props.componentsRemove(pluck('id', itemsToRemove));
			}
		}
	}

	getWrapProps () {
		return this.props;
	}

	debugInfo (debugMode = false) {
		if (!debugMode) {
			return null;
		}

		if (!containers[this.props.id]) {
			containers[this.props.id] = 0;
		}

		return ` [I-${this.props.id}(${containers[this.props.id]++})] `;
	}

	render () {
		const Wrap = this.props.Wrap;
		const Item = this.props.Item;
		// TODO: use container only if needed (for option in select is container not needed)
		//const items = this.props.items.map(item => <Container key={item.id} id={item.id} childConfigs={item.childConfigs} Wrap={Item} />);
		const items = this.props.items.map(item => <Item key={item.id} id={item.id} dataPath={item.dataPath} attributes={item.attributes} childConfigs={item.childConfigs} getWrapProps={this.getWrapProps} />);

		return (<React.Fragment>
			{this.debugInfo()}
			<Wrap {...this.props}>
				{items}
			</Wrap>
		</React.Fragment>);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		items: getComponentChildren(state, ownProps.id)
	};
};

const mapDispatchToProps = dispatch => ({
	componentsInit: (configs, id) => dispatch(componentsInit(configs, id, true)),
	componentsRemove: (ids) => dispatch(componentsRemove(ids))
});

export default connect(mapStateToProps, mapDispatchToProps)(Items);
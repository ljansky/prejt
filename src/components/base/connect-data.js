import { connect } from 'react-redux';
import { collectDataForComponent } from '../../store/data/data.selectors';

export default (mapDataToProps = [], options = {}) => {
	const mapStateToProps = (state, ownProps) => ({
		data: collectDataForComponent(state, ownProps, mapDataToProps)
	});

	//const mapDispatchToProps = (dispatch, ownProps) => options.mapDispatchToProps ? options.mapDispatchToProps(dispatch, ownProps) : {};
	
	return connect(mapStateToProps);
};

import { concat } from 'ramda';
import connectBase from './connect-base';
import { getDataPath } from '../../utils/data';
import { dataSet } from '../../store/data/data.actions';

const mapDispatchToProps = (dispatch, ownProps) => ({
	onChange: (event) => {
		console.log('CHANGE', event);
		const path = getDataPath(ownProps.attributes.value, ownProps.dataPath);
		dispatch(dataSet(path, event.target.value));
	}
});

export default (Component, mapDataToProps = []) => {
	const dataToProps = concat(['value'], mapDataToProps);
	return connectBase(Component, dataToProps, { mapDispatchToProps });
};
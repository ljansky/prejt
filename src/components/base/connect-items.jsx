import React from 'react';
import connectBase from './connect-base';
import Items from './items';

export default (Wrap, Item) => {
	const BaseComponent = props => (<Items {...props} Wrap={Wrap} Item={Item} />);
	return connectBase(BaseComponent, ['items']);
};
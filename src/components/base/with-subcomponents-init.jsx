import { componentsInit } from '../../store/components/components.actions';

export default {
	dispatchToProps: dispatch => ({
		componentsInit: (components, id) => dispatch(componentsInit(components, id, true))
	})
};

import React from 'react';
import { getWrapProps } from '../../utils/components';

/**
 * Base component for most of other components. It is used inside other components to wrap the content of the component.
 * It adds the functionality to set layout of component - width. Width is set in component definition in XML file of module as parameter. Its a number from 1 to 12
 * and it means the same as col-md-{number} class in bootstrap framework
 */
class Base extends React.Component {

	isEnabled () {
		return (typeof this.props.attributes.enabled === 'undefined') || this.props.data.enabled;
	}

	render () {
		if (!this.isEnabled()) {
			return null;
		} else {
			if (this.props.Wrap) {
				const Wrap = this.props.Wrap;
				const props = getWrapProps(this.props);

				return (<Wrap {...props}>{this.props.children}</Wrap>);
			} else {
				return (<React.Fragment>{this.props.children}</React.Fragment>);
			}
		}
	}
}

export default Base;
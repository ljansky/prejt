import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { Items } from './items';

const mockStore = configureStore();
const initialState = {};
const store = mockStore(initialState);

describe('items', () => {

	const Wrap = props => (<ul className='wrap'>{props.children}</ul>);
	const Item = props => (<li className='item'>test</li>);

	const props = {
		items: [{
			childConfigs: []
		}, {
			childConfigs: []
		}],
		Wrap,
		Item
	};

	it('should render component', () => {
		const wrapper = mount(<Items {...props} />);

		expect(wrapper.find('ul.wrap')).toHaveLength(1);
		expect(wrapper.find('li.item')).toHaveLength(2);
	});
});
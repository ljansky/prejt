import React from 'react';

const DefaultWrap = props => {
	return <div style={props.getStyles && props.getStyles()} className={props.className}>{props.children}</div>;
};

export default DefaultWrap;
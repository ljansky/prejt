import { connect } from 'react-redux';
import { getDataPath } from '../../utils/data';
import { dataSet } from '../../store/data/data.actions';

export default {
	component: (Component) => {
		const mapDispatchToProps = (dispatch, ownProps) => ({
			onChange: (event, changePath = null) => {
				const path = getDataPath(changePath || ownProps.attributes.value, ownProps.dataPath);
				dispatch(dataSet(path, event.target.value));
			}
		});
		
		return connect(null, mapDispatchToProps)(Component);
	},
	dataToProps: ['value']
};
import React from 'react';
import { connect } from 'react-redux';
import Container from './base/container';
import { moduleUpdate } from '../store/module/module.actions';

class Module extends React.Component {

	componentDidMount () {
		this.props.moduleUpdate(this.props.match.params.module, this.props.match.params.id);
	}

	componentDidUpdate (props) {
		const moduleChange = this.props.match.params.module !== props.match.params.module;
		const idChange = this.props.match.params.id !== props.match.params.id;
		if (moduleChange || idChange) {
			this.props.moduleUpdate(this.props.match.params.module, this.props.match.params.id);
		}
	}

	render () {
		const data = {
			module: 'main'
		};

		return <Container id={0} data={data} />;
	}
}

const mapDispatchToProps = dispatch => ({
	moduleUpdate: (module, id) => dispatch(moduleUpdate(module, id))
});

export default connect(null, mapDispatchToProps)(Module);
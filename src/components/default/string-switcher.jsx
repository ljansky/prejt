import React from 'react';

import { composePrejt, Container } from '../base';

const StringSwitcherWrap = props => (<React.Fragment>{props.children}</React.Fragment>);

class StringSwitcher extends React.Component {
	render () {
		const props = this.props;
		if (props.data.visible) {
			return (<Container{...props} id={props.id} childConfigs={props.childConfigs} Wrap={StringSwitcherWrap} />);
		} else {
			return props.data.value || null;
		}
	}
}

export default composePrejt(
	['module', 'value' ,'visible']
)(StringSwitcher);
import React from 'react';

import { composePrejt, withInput, withBase } from '../base';

class SetValue extends React.Component {

	componentDidMount () {
		const value = this.props.data.from;
		this.props.onChange({ target: { value } });
	}

	componentDidUpdate (props) {
		if (this.props.data.from !== props.data.from) {
			const value = this.props.data.from;
			this.props.onChange({ target: { value } });
		}
	}

	render () {
		return null;
	}
}

export default composePrejt(
	['from'],
	withBase,
	withInput
)(SetValue);
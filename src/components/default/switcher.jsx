import React from 'react';

import { composePrejt, Container } from '../base';

const SwitcherWrap = props => (<React.Fragment>{props.children}</React.Fragment>);

class Switcher extends React.Component {
	render () {
		const props = this.props;
		console.log('SWITCHER', props.childConfigs);
		if (props.data.switched) {
			return (<Container{...props} id={props.id} childConfigs={props.childConfigs} Wrap={SwitcherWrap} />);
		} else {
			return props.data.value;
		}
	}
}

export default composePrejt(
	['switched']
)(Switcher);
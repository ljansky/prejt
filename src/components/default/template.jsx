import React from 'react';
import { reduce, assoc, keys } from 'ramda';

import { composePrejt, Container } from '../base';

const templateVarPrefix = 'data-';

const getTemplateVariables = attributes => reduce((acc, curr) => {
	if (curr.startsWith(templateVarPrefix)) {
		return assoc(curr.substr(templateVarPrefix.length), attributes[curr], acc);
	}
	return acc;
}, {}, keys(attributes));

const TemplateWrap = props => (<React.Fragment>{props.children}</React.Fragment>);

class Template extends React.Component {

	constructor (props) {
		super(props);
		this.state = {
			templateVariables: getTemplateVariables(props.attributes)
		};
	}

	render () {
		const props = this.props;
		return (<Container {...props} id={props.id} childConfigs={props.childConfigs} Wrap={TemplateWrap} templateVariables={this.state.templateVariables} />);
	}
}

export default composePrejt(
	['module']
)(Template);
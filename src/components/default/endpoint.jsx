import React from 'react';
import { reduce, assoc, equals, any, pipe, filter, values } from 'ramda';
import { connect } from 'react-redux';
import { dataFetch } from '../../store/data/data.actions';
import { componentRemove } from '../../store/components/components.actions';
import { getDataFromStore } from '../../store/data/data.selectors';
import { getDataPath } from '../../utils/data/get-data-path';

const collectValues = type => (configs, dataPath = []) => state => pipe(
	filter(item => item.name === type),
	reduce((acc, curr) => assoc(curr.attributes.name, getDataFromStore(state, curr.attributes.value, dataPath), acc), {})
)(configs);

const collectParams = collectValues('param');
const collectQuery = collectValues('query');

class Endpoint extends React.Component {

	componentDidMount () {
		this.fetchData();
	}

	componentDidUpdate (prevProps) {
		if (!equals(prevProps.params, this.props.params) || !equals(prevProps.query, this.props.query)) {
			this.fetchData();
		}
	}

	fetchData () {
		if (this.props.enabled) {
			const hasNullParam = any(value => value === null, values(this.props.params));
			const hasNullQuery = any(value => value === null, values(this.props.query));
			if (!hasNullParam && !hasNullQuery) {
				this.props.fetch(this.props.params, this.props.query);
			}
		}
	}

	componentWillUnmount () {
		this.props.unmount();
	}

	render () {
		return null;
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		params: collectParams(ownProps.childConfigs, ownProps.dataPath)(state),
		query: collectQuery(ownProps.childConfigs, ownProps.dataPath)(state),
		enabled: ownProps.attributes.enabled ? getDataFromStore(state, ownProps.attributes.enabled, ownProps.dataPath) : true
	};
};

const mapDispatchToProps = (dispatch, ownProps) => ({
	fetch: (params, query) => dispatch(dataFetch({
		fetch: ownProps.attributes.fetch,
		dataPath: getDataPath(ownProps.attributes.store || '.', ownProps.dataPath),
		params,
		query
	})),
	unmount: () => dispatch(componentRemove(ownProps.id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Endpoint);
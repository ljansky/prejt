import React from 'react';
import { composePrejt, Container, withItems } from '../base';

const ForEachWrap = props => (<React.Fragment>{props.children}</React.Fragment>);

const ForEachItemWrap = props => (<React.Fragment>{props.children}</React.Fragment>);

const ForEachItem = props => (<Container {...props} id={props.id} childConfigs={props.childConfigs} Wrap={ForEachItemWrap} />)

export default composePrejt(
	withItems(ForEachItem)
)(ForEachWrap);
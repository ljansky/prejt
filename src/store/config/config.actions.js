export const CONFIG_INIT = 'CONFIG_INIT';

export const configInit = (config) => ({
	type: CONFIG_INIT,
	config
});
import { CONFIG_INIT } from './config.actions';

const initialState = {
	baseUrl: null
};

export default (state = initialState, action) => {

	if (action.type === CONFIG_INIT) {
		return action.config;
	}

	return state;
};

import { ofType } from 'redux-observable';
import { map, mergeMap, tap } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { COMPONENTS_FETCH, componentsInit } from './components.actions';
import getComponentConfigs from '../../utils/components/get-components-configs';

const moduleCache = {};

const fetchXmlModule = (path) => {
	console.log('FETCH XML', path);
	if (!moduleCache[path]) {
		moduleCache[path] = fetch(path)
			.then(res => res.text());
	}

	return moduleCache[path];
};

const configsCache = {};

const getCachedComponentConfigs = (path, templateVariables, action) => {
	const cacheId = JSON.stringify({ path, templateVariables });
	
	if (!configsCache[cacheId]) {
		configsCache[cacheId] = fetchXmlModule(path)
			.then(configString => getComponentConfigs(templateVariables)(configString));
	}
	console.log('GET CONFIG', path, configsCache[cacheId]);
	return configsCache[cacheId]
		.then(moduleConfig => {
			console.log('AFTER PROMISE', moduleConfig);
			return moduleConfig;
		})
		.then(moduleConfig => ({
			moduleConfig,
			action
		}));
};

export const fetchModule = actions$ =>
	actions$.pipe(
		ofType(COMPONENTS_FETCH),
		mergeMap(action => {
			return from(getCachedComponentConfigs(action.path, action.templateVariables, action))
				.pipe(
					map(actionConfig => {
						console.log('GGG', actionConfig);
						return actionConfig;
					})
				);
		}),
		tap(actionConfig => {
			console.log('FETCHED', actionConfig);
		}),
		map((actionConfig) => componentsInit(
			actionConfig.moduleConfig.children,
			actionConfig.action.parentId,
			false,
			actionConfig.moduleConfig.moduleAttributes
		))
	);
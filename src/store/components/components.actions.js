import getComponentConfigs from '../../utils/components/get-components-configs';

export const COMPONENTS_FETCH = 'components_fetch';
export const COMPONENTS_INIT = 'components_init';
export const COMPONENT_REMOVE = 'COMPONENT_REMOVE';
export const COMPONENTS_REMOVE = 'COMPONENTS_REMOVE';
export const COMPONENT_RESOLVE = 'COMPONENT_RESOLVE';

/*export const componentsFetch = (path, parentId = null, templateVariables = {}) => ({
	type: COMPONENTS_FETCH,
	path,
	parentId,
	templateVariables
});*/

const moduleCache = {};

const fetchXmlModule = (path) => {
	if (!moduleCache[path]) {
		moduleCache[path] = fetch(path)
			.then(res => res.text());
	}

	return moduleCache[path];
};

const configsCache = {};

const getCachedComponentConfigs = (path, templateVariables) => {
	const cacheId = JSON.stringify({ path, templateVariables });
	
	if (!configsCache[cacheId]) {
		configsCache[cacheId] = fetchXmlModule(path)
			.then(configString => getComponentConfigs(templateVariables)(configString));
	}
	return configsCache[cacheId];
};

export const componentsFetch = (path, parentId = null, templateVariables = {}) => {
	return dispatch => {
		getCachedComponentConfigs(path, templateVariables)
			.then(moduleConfig => {
				const action = componentsInit(
					moduleConfig.children,
					parentId,
					false,
					moduleConfig.moduleAttributes
				);
				dispatch(action);
			});
	};
};

export const componentsInit = (configs, parentId = null, append = false, moduleAttributes = {}) => {
	return {
		type: COMPONENTS_INIT,
		configs,
		parentId,
		append,
		moduleAttributes
	};
};

export const componentRemove = (id) => ({
	type: COMPONENT_REMOVE,
	id
});

export const componentsRemove = (ids) => ({
	type: COMPONENTS_REMOVE,
	ids
});

export const componentResolve = (id, resolved = true) => ({
	type: COMPONENT_RESOLVE,
	id,
	resolved
});
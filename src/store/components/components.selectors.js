import createCachedSelector from 're-reselect';
import { map, pipe, filter } from 'ramda';

const getComponentChildren = (components, id) => {
	const component = components[id];
	return pipe(
		map(childId => components[childId] || null),
		filter(child => child !== null)
	)(component ? component.children : []);
};

export default createCachedSelector(
	state => state.components.index,
	(state, id) => id,
	getComponentChildren
)((state, id) => id);
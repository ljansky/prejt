// @flow

import { merge, concat, assoc, map, dissoc, omit } from 'ramda';
import { COMPONENTS_INIT, COMPONENT_REMOVE, COMPONENTS_REMOVE, COMPONENT_RESOLVE } from './components.actions';
import { mapListToObject, mapCounter, splitDrop, splitActionString } from '../../utils/common';
import type { ComponentsState } from './components.model';

/**
 * Reducer for components. This part of store is for components info. If any component has state, it would be good to be stored here. Components are stored under its id which is generated in componentLoader.
 * Compoent also can have child components - they are stored in children array of component state and are ids of component in this part of store.
 * @param  {Object} state
 * @param  {string} action
 * @return {Object}
 */

const initialState: ComponentsState = {
	index: {},
	lastId: 0
};

const getDataPath = (parentDataPath, itemDataPath) => {
	if (itemDataPath) {
		if (itemDataPath.charAt(0) === '/') {
			return splitDrop(itemDataPath);
		} else if (itemDataPath.charAt(0) === '.') {
			return parentDataPath ? concat(parentDataPath, splitDrop(itemDataPath)) : null;
		} else {
			return null;
		}
	} else {
		return parentDataPath;
	}
};

export default (state: ComponentsState = initialState, action: any) => {

	if (action.type === COMPONENTS_INIT) {
		const parent: any = state.index[action.parentId || 0] || { id: 0 };
		const parentDataPath = parent && parent.dataPath || null;
		
		const configs = mapCounter((item, id) => merge(item, {
			id,
			resolved: !item.attributes || !item.attributes.module,
			parentId: action.parentId || 0,
			children: [],
			dataPath: getDataPath(parentDataPath, item.attributes && item.attributes.data)
		}), state.lastId, action.configs);

		const newChildren = map(item => item.id, configs.list);

		const updatedParent = merge(parent, {
			children: concat(action.append && parent.children || [], newChildren),
			resolved: true,
			moduleAttributes: action.moduleAttributes || {}
		});

		const updateIndex = concat(configs.list, [updatedParent]);

		return merge(state, {
			index: mapListToObject('id', state.index, updateIndex),
			lastId: configs.lastId
		});
	}

	if (action.type === COMPONENT_REMOVE) {
		// TODO: remove from parents children
		const updatedIndex = dissoc(action.id, state.index);
		return assoc('index', updatedIndex, state);
	}

	if (action.type === COMPONENTS_REMOVE) {
		// TODO: remove from parents children
		const updatedIndex = omit(action.ids, state.index);
		return assoc('index', updatedIndex, state);
	}

	if (action.type === COMPONENT_RESOLVE) {
		const component = assoc('resolved', action.resolved, state.index[action.id]);
		const updatedIndex = assoc(action.id, component, state.index);
		return assoc('index', updatedIndex, state);
	}

	return state;
};

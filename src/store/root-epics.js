import { combineEpics } from 'redux-observable';
import { fetchModule } from './components/components.epics';
import { dataFetch } from './data/data.epics';
import { actionCallEpic } from './actions/actions.epics';

export default combineEpics(
	// fetchModule,
	dataFetch,
	actionCallEpic
);
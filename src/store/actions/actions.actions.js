import { drop } from 'ramda';
export const ACTION_CALL = 'ACTION_CALL';

export const actionCall = (actions, callerProps, previousActionResult = null) => ({
	type: ACTION_CALL,
	actions,
	callerProps,
	previousActionResult
});

export const actionCallNext = (actionCallData, actionResult) => {
	const result = typeof actionResult === 'undefined' ? actionCallData.previousActionResult : actionResult;
	const remainingActions = drop(1, actionCallData.actions);
	return actionCall(remainingActions, actionCallData.callerProps, result);
};

export const actionNoop = () => ({
	type: 'NOOP'
});
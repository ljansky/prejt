import { pipe } from 'rxjs';
import { map, tap } from 'rxjs/operators';

export default pipe(
	tap(action => {
		console.log('PREVIOUS ACTION RESULT', action.previousActionResult);
	}),
	map(action => {
		return {
			result: action.previousActionResult ? action.previousActionResult + 'f' : 'g'
		};
	})
);
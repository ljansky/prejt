import { dataSet } from '../../data/data.actions';
import { map } from 'rxjs/operators';
import { actionCallNext } from '../actions.actions';

export default map(action => {
	const path = action.params[0].path;
	const value = action.params[1] ? action.params[1].value : action.previousActionResult;
	return [
		dataSet(path, value),
		actionCallNext(action, value)
	];
});
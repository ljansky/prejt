import { map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { getDataFromStore } from '../../data/data.selectors';
import { fetchByOptions } from '../../data/data.epics';
import { actionCallNext } from '../actions.actions';

export default map(action => {
	const data = action.params[1] ? action.params[1].value : getDataFromStore(action.state$.value, '.', action.callerProps.dataPath);
	return [
		of({
			fetch: action.params[0].value,
			method: 'DELETE',
			params: data,
			state: action.state$.value
		}).pipe(
			mergeMap((fetchOptions) => fetchByOptions({ value: fetchOptions.state} )(fetchOptions)),
			map(result => actionCallNext(action, result.data))
		)
	];
});
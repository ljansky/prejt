import { map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { filter, is } from 'ramda';
import { getDataFromStore } from '../../data/data.selectors';
import { fetchByOptions } from '../../data/data.epics';
import { actionCallNext } from '../actions.actions';

export default map(action => {
	const rawData = action.params[1] ? action.params[1].value : getDataFromStore(action.state$.value, '.', action.callerProps.dataPath);
	const data = filter(val => is(Number, val) || is(String, val) || is(Boolean, val), rawData);

	return [
		of({
			fetch: action.params[0].value,
			method: 'PUT',
			data,
			params: data,
			state: action.state$.value
		}).pipe(
			mergeMap((fetchOptions) => fetchByOptions({ value: fetchOptions.state} )(fetchOptions)),
			map(result => actionCallNext(action, result.data))
		)
	];
});
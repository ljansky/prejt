import { dataUnset } from '../../data/data.actions';
import { map } from 'rxjs/operators';
import { actionCallNext } from '../actions.actions';

export default map(action => {
	const path = action.params[0].path;
	return [
		dataUnset(path),
		actionCallNext(action)
	];
});
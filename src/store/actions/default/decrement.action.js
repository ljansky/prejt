import { dataSet } from '../../data/data.actions';
import { map } from 'rxjs/operators';
import { actionCallNext } from '../actions.actions';

export default map(action => {
	const path = action.params[0].path;
	const value = action.params[0].value || 0;
	const incrementValue = action.params[1] && parseInt(action.params[1].value) || 1;
	const result = value-incrementValue;
	return ([
		dataSet(path, result),
		actionCallNext(action, result)
	]);
});
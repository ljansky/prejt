import { dataSet } from '../../data/data.actions';
import { map } from 'rxjs/operators';
import { actionCallNext } from '../actions.actions';
import { drop, split } from 'ramda';

export default map(action => {
	let modalProps = action.callerProps;
	while (modalProps.parentId !== 0) {
		modalProps = action.state$.value.components.index[modalProps.parentId];
	}

	if (modalProps.name === 'modal') {
		const enabledPath = drop(1, split('/', modalProps.attributes.enabled));
		return ([
			dataSet(enabledPath, false),
			actionCallNext(action)
		]);
	}

});
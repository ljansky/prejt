import { dataSet } from '../../data/data.actions';
import { map } from 'rxjs/operators';
import { path, append } from 'ramda';
import { actionCallNext } from '../actions.actions';

export default map(action => {
	const dataPath = action.params[0].path;
	const data = path(dataPath, action.state$.value.data);
	const newData = append({}, data);

	return [
		dataSet(dataPath, newData),
		actionCallNext(action, newData)
	];
});
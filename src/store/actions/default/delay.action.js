import { of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { actionCallNext } from '../actions.actions';

export default map(action => ([
	of(action).pipe(
		delay(action.params[0] && action.params[0].value || 1000),
		map(actionCallNext)
	)
]));
import { dataSet, dataUnset } from '../../data/data.actions';
import { map, filter, take, tap } from 'rxjs/operators';
import { path } from 'ramda';
import { actionCallNext, actionNoop } from '../actions.actions';
import { componentsInit } from '../../components/components.actions';
import uuid from 'uuid';

export default map(action => {
	const modalModule = action.params[0].value;
	const modalId = uuid();
	const dataPath = typeof action.params[1] !== 'undefined' ? action.params[1].path : ['modals', modalId];
	const enabledPath = ['modals', modalId, 'enabled'];
	return [
		componentsInit([{
			name: 'modal',
			attributes: {
				module: modalModule,
				data: '/' + dataPath.join('/'),
				enabled: '/' + enabledPath.join('/')
			},
			childConfigs: []
		}], 0, true),
		dataSet(enabledPath, true),
		action.state$.pipe(
			filter(state => path(['data', ...enabledPath], state) === false),
			map(path(['data', ...dataPath])),
			tap(result => console.log('RESULT', result)),
			map(result => result ? actionCallNext(action, result) : actionNoop()),
			take(1)
		),
		dataUnset(['modals', modalId])
	];
});
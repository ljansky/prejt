import { dataSet } from '../../data/data.actions';
import { map } from 'rxjs/operators';
import uuid from 'uuid';
import { actionCallNext } from '../actions.actions';

export default map(action => {
	const path = action.params[0].path;
	const value = uuid();

	return [
		dataSet(path, value),
		actionCallNext(action, value)
	];
});
import { push } from 'connected-react-router';
import { map } from 'rxjs/operators';
import { join, concat, map as rMap } from 'ramda';
import { actionCallNext } from '../actions.actions';

export default map(action => ([
	push(join('/', concat([''], rMap(item => item.value, action.params)))),
	actionCallNext(action)
]));
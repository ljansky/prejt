import { dataSet } from '../../data/data.actions';
import { map } from 'rxjs/operators';
import { lensPath, set } from 'ramda';
import { actionCallNext } from '../actions.actions';

export default map(action => {
	const path = action.params[0].path;
	const permanentDataString = localStorage.getItem('permanentData');
	const permanentData = permanentDataString ? JSON.parse(permanentDataString) : {};
	const updatedPermanentData = set(lensPath(path), action.previousActionResult, permanentData);
	localStorage.setItem('permanentData', JSON.stringify(updatedPermanentData));
	return ([
		dataSet(path, action.previousActionResult),
		actionCallNext(action)
	]);
});
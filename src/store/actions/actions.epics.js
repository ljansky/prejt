import { ofType } from 'redux-observable';
import { mergeMap, filter, catchError } from 'rxjs/operators';
import { concat, pipe, of, isObservable } from 'rxjs';
import { ACTION_CALL } from './actions.actions';
import { head, drop, map, split, pipe as rPipe, path } from 'ramda';
import { getDataFromStore } from '../data/data.selectors';
import * as defaultActions from './default';
import { getDataPath } from '../../utils/data';

const getPrevParam = (param, previousActionResult) => {
	const paramPath = rPipe(
		split('/'),
		drop(1)
	)(param);

	return {
		path: [],
		value: path(paramPath, previousActionResult)
	};
};

const actionsToObservables = map(action => isObservable(action) ? action : of(action));

const getParamValue = ({ callerProps, previousActionResult }, state) => param => {
	return (param.charAt(0) === '_' ? getPrevParam(param, previousActionResult) : {
		path: getDataPath(param, callerProps.dataPath),
		value: getDataFromStore(state, param, callerProps.dataPath)
	});
};

const getActionObservable = state$ => actionCallData => {
	const actionInfo = head(actionCallData.actions);
	const action = defaultActions[actionInfo.name];
	const actionParams = map(getParamValue(actionCallData, state$.value))(actionInfo.params);

	return of({
		params: actionParams,
		state$,
		callerProps: actionCallData.callerProps,
		previousActionResult: actionCallData.previousActionResult,
		actions: actionCallData.actions
	}).pipe(
		action,
		mergeMap(actions => concat(...actionsToObservables(actions)))
	);
};

export const actionCallEpic = (actions$, state$) =>
	actions$.pipe(
		ofType(ACTION_CALL),
		filter(actionCallData => actionCallData.actions.length > 0),
		mergeMap(actionCallData => pipe(
			getActionObservable(state$),
			catchError(error => of({ type: 'ERROR', error }))
		)(actionCallData))
	);
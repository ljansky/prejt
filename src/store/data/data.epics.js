import { ofType } from 'redux-observable';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { DATA_FETCH, dataSet } from './data.actions';
import UrlPattern from 'url-pattern';
import axios from 'axios';

const replaceParams = params => url => {
	const pattern = new UrlPattern(url);
	return pattern.stringify(params);
};

export const fetchByOptions = state => options => {
	const url = replaceParams(options.params || {})(options.fetch);
	const baseUrl = url.charAt(0) === '/' ? '' : state.value.config.baseUrl;

	const headers = state.value.data.user ? {
		'Authorization': 'Bearer ' + state.value.data.user.token
	} : {};

	return axios({
		method: options.method || 'GET',
		url: baseUrl + url,
		params: options.query || {},
		data: options.data || {},
		headers
	}).then(data => ({
		data: data.data,
		options
	}));
};

export const dataFetch = (actions$, state) =>
	actions$.pipe(
		ofType(DATA_FETCH),
		map(action => action.options),
		mergeMap(fetchByOptions(state)),
		map(({ data, options }) => dataSet(options.dataPath, data)),
		catchError(error => (of({ type: 'ERROR', error })))
	);

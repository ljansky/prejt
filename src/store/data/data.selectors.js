import createCachedSelector from 're-reselect';
import { view, lensPath, join, reduce, assoc, equals, drop, contains, pluck } from 'ramda';
import { getDataPath, getTransformations, applyTransformations } from '../../utils/data';

export const getDataByDataPath = createCachedSelector(
	state => state,
	(state, path) => path,
	(data, path) => view(lensPath(path), data)
)((state, path) => join('/', path));

export const getRawDataByPath = (state, attributePath, relativeToPath) => {
	if (attributePath.charAt(0) === '$') {
		return state.module[drop(1, attributePath)];
	} else {
		const path = getDataPath(attributePath, relativeToPath);
		return getDataByDataPath(state.data, path);
	}
};

export const getDataFromStore = (state, dataAttribute, relativeToPath) => {
	if (typeof dataAttribute !== 'undefined') {
		if (contains(dataAttribute.charAt(0), ['$', '.', '/'])) {
			const pathWithTransformations = getTransformations(dataAttribute);
			const data = getRawDataByPath(state, pathWithTransformations.path, relativeToPath);
			return applyTransformations(pathWithTransformations.transformations)(data);
		}

		return dataAttribute;
	}

	return null;
};

export const getControlByPath = (state, path) => {
	const pathString = join('/', path);
	return state.data.control[pathString];
};

const dataCache = {};

export const collectDataForComponent = (state, ownProps, mapDataToProps) => {
	const data = reduce((acc, curr) => {
		if (typeof ownProps.attributes[curr] !== 'undefined') {
			const value = getDataFromStore(state, ownProps.attributes[curr], ownProps.dataPath);
			const path = contains(ownProps.attributes[curr].charAt(0), ['.', '/'])
				? getDataPath(ownProps.attributes[curr], ownProps.dataPath)
				: ownProps.attributes[curr];

			return assoc(curr, { value, path }, acc);
		}

		return acc;
	}, {})(mapDataToProps);

	const values = pluck('value', data);
	const paths = pluck('path', data);

	if (!dataCache[ownProps.id] || !equals(dataCache[ownProps.id].values, values)) {
		dataCache[ownProps.id] = { values, paths };
	}

	return dataCache[ownProps.id];
};

export const DATA_SET = 'DATA_SET';
export const DATA_UNSET = 'DATA_UNSET';
export const DATA_FETCH = 'DATA_FETCH';

export const dataFetch = (options = {}) => {
	return {
		type: DATA_FETCH,
		options
	};
};

export const dataSet = (path, value) => {
	return {
		type: DATA_SET,
		path,
		value
	};
};

export const dataUnset = (path) => ({
	type: DATA_UNSET,
	path
});
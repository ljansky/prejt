import { DATA_SET, DATA_UNSET } from './data.actions';
import { assocPath, dissocPath } from 'ramda';

const initialState = {};

export default (state = initialState, action) => {
	if (action.type === DATA_SET) {
		return action.path ? assocPath(action.path, action.value, state) : state;
	}

	if (action.type === DATA_UNSET) {
		return action.path ? dissocPath(action.path, state) : state;
	}

	return state;
};

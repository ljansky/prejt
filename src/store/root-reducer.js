import { combineReducers } from 'redux';
import components from './components/components.reducer';
import module from './module/module.reducer';
import data from './data/data.reducer';
import config from './config/config.reducer';

const rootReducer = combineReducers({
	components,
	module,
	data,
	config
});

export default rootReducer;
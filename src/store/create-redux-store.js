// @flow

import { createStore, applyMiddleware, compose } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import thunk from 'redux-thunk';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import rootReducer from './root-reducer';
import rootEpics from './root-epics';
import { configInit } from './config/config.actions';

export const createReduxStore = (config, history) => {
	const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
	const epicMiddleware = createEpicMiddleware();
	
	const permanentStoreData = localStorage.getItem('permanentData');

	const store = createStore(
		connectRouter(history)(rootReducer),
		{
			data: permanentStoreData ? JSON.parse(permanentStoreData) : {}
		},
		composeEnhancers(
			applyMiddleware(
				routerMiddleware(history),
				thunk,
				epicMiddleware
			)
		)
	);

	epicMiddleware.run(rootEpics);
	store.dispatch(configInit(config));

	return store;
};
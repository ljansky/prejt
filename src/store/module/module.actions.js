export const MODULE_UPDATE = 'module-update';

export const moduleUpdate = (module = null, id = null) => ({
	type: MODULE_UPDATE,
	module,
	id
});
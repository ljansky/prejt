// @flow

export type ModuleState = {
	module: string | null;
	id: number | null;
};
// @flow

import { MODULE_UPDATE } from './module.actions';
import type { ModuleState } from './module.model';

const initialState: ModuleState = {
	module: null,
	id: null
};

export default (state: ModuleState = initialState, action: any) => {

	if (action.type === MODULE_UPDATE) {
		return {
			module: action.module,
			id: action.id === null ? null : parseInt(action.id)
		};
	}

	return state;
};

import { split, drop, pipe, map } from 'ramda';
import * as transformations from './transformations';
import { parseFunction } from '../common';

const getTransformations = path => {
	const parts = split('|', path);
	return {
		path: parts[0],
		transformations: pipe(
			drop(1),
			map(transformation => {
				const tr = parseFunction(transformation);
				return {
					func: transformations[tr.name],
					params: tr.params
				};
			})
		)(parts)
	};
};

export default getTransformations;
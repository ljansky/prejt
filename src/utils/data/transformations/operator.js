const operator = (input, params) => {
	return `${params[0]}(${input})`;
};

export default operator;
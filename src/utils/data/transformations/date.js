const date = input => {
	const output = input ? new Date(input) : new Date();

	output.setMinutes(output.getMinutes() - output.getTimezoneOffset());

	return output.toJSON().slice(0, 10);
};

export default date;
const route = (input) => {
	return input ? `${input}/index` : null;
};

export default route;
const defined = input => {
	return typeof input !== 'undefined';
};

export default defined;
const empty = input => {
	return (typeof input === 'undefined') || (input === null) || (input === '');
};

export default empty;
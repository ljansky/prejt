const not = input => {
	return !input;
};

export default not;
import { pipe, split, map, drop, join } from 'ramda';
import numbersToInt from '../common/numbers-to-int';
import path from 'path';

const splitDataPath = pipe(
	split('/'),
	drop(1),
	map(numbersToInt)
);

export const getDataPath = (dataPathString, parentDataPath) => {
	const firstChar = dataPathString && dataPathString.charAt(0);
	if (firstChar === '.' || firstChar === '/') {
		const parentString = '/' + join('/', parentDataPath || []);
		const resolvedPathString = path.resolve(parentString, dataPathString);
		return splitDataPath(resolvedPathString);
	} else {
		return null;
	}
};

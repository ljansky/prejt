import { reduce, curry } from 'ramda';

const applyTransformations = (transformations, value) => {
	return reduce((acc, curr) => curr.func(acc, curr.params), value, transformations || []);
};

export default curry(applyTransformations);
import { omit, merge, pipe } from 'ramda';

export default (props, mergeValues = {} ) => pipe(
	props => merge(props, mergeValues),
	omit(['Wrap'])
)(props);
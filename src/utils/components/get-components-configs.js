import parseXml from '@rgrove/parse-xml';
import { pipe, prop, head, filter, whereEq, map, mapObjIndexed } from 'ramda';

const setTemplateVariables = templateVariables => attributes => {
	if (Object.keys(templateVariables).length) {
		return mapObjIndexed((val) => {
			if (val.charAt(0) === '@' && templateVariables[val.substr(1)]) {
				return templateVariables[val.substr(1)];
			}

			let retVal = val;
			for (let i in templateVariables) {
				retVal = retVal.replace(`{${i}}`, templateVariables[i]);
			}

			return retVal;
		}, attributes);
	}
	
	return attributes;
};

const getChildElements = templateVariables => pipe(
	prop('children'),
	filter(whereEq({ type: 'element' })),
	map(config => ({
		name: config.name,
		attributes: setTemplateVariables(templateVariables)(config.attributes),
		childConfigs: getChildElements(templateVariables)(config)
	}))
);

const getModuleConfig = templateVariables => pipe(
	prop('children'),
	head,
	moduleConfig => ({
		children: getChildElements(templateVariables)(moduleConfig),
		moduleAttributes: moduleConfig.attributes
	})
);

export default templateVariables => pipe(
	parseXml,
	getModuleConfig(templateVariables)
);
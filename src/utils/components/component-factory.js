import React from 'react';
import { merge, map } from 'ramda';
import * as baseComponents from '../../components/base';
import * as defaultComponents from '../../components/default';

let components = defaultComponents;

const getComponentClassName = name => {
	return name
		.split('-')
		.map(partString => partString.charAt(0).toUpperCase() + partString.slice(1))
		.join('');
};

export const getComponent = config => {
	const componentClass = components[getComponentClassName(config.name)] || null;
	return componentClass ? React.createElement(componentClass, merge(config, { key: config.id })) : null;
};

export const addComponents = componentsToBeAdded => {
	const prejtExpose = merge(baseComponents, defaultComponents);
	const c = map(cf => cf(prejtExpose), componentsToBeAdded);
	components = merge(components, c);
};
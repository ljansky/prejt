export { getComponent, addComponents } from './component-factory';
export { default as getComponentsConfigs } from './get-components-configs';
export { default as getWrapProps } from './get-wrap-props';
const numbersToInt = str => {
	const result = /^\+?([0-9]\d*)$/.test(str) ? parseInt(str) : str;
	return result;
};

export default numbersToInt;
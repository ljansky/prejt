import { split, pipe, map } from 'ramda';
import parseFunction from './parse-function';

const splitActionString = pipe(
	split('|'),
	map(parseFunction)
);

export default splitActionString;
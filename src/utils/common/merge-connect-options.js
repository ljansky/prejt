import { merge } from 'ramda';

const mergeConnectOptions = optionsToMerge => options => {

	const mapStateToProps = (state, ownProps) => merge(
		options.mapStateToProps ? options.mapStateToProps(state, ownProps) : {},
		optionsToMerge.mapStateToProps ? optionsToMerge.mapStateToProps(state, ownProps) : {}
	);

	const mapDispatchToProps = (dispatch, ownProps) => merge(
		options.mapDispatchToProps ? options.mapDispatchToProps(dispatch, ownProps) : {},
		optionsToMerge.mapDispatchToProps ? optionsToMerge.mapDispatchToProps(dispatch, ownProps) : {}
	);

	return merge(options, { mapStateToProps, mapDispatchToProps });
};

export default mergeConnectOptions;
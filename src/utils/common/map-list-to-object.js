import { curry, reduce, assoc } from 'ramda';

const mapListToObject = (key, initialObject, list) => reduce(
	(acc, curr) => assoc(curr[key], curr, acc),
	initialObject,
	list
);

export default curry(mapListToObject);
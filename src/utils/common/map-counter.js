// @flow

import { curry, reduce, append } from 'ramda';

const mapCounter = (mapFunction, initialId, list) => reduce((acc, curr) => {
	return {
		list: append(mapFunction(curr, acc.lastId + 1), acc.list),
		lastId: acc.lastId + 1
	};
}, {
	list: [],
	lastId: initialId
}, list);

export default curry(mapCounter);
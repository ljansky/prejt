import { split, pipe, drop, map } from 'ramda';
import numbersToInt from './numbers-to-int';

const splitDrop = pipe(
	split('/'),
	drop(1),
	map(numbersToInt)
);

export default splitDrop;
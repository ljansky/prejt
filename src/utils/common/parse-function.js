const parseFunction = functionString => {
	const result = /([^(]+?)\(\s*([^)]+?)\s*\)/.exec(functionString);
	return {
		name: result ? result[1] : functionString,
		params: result && result[2] ? result[2].split(/\s*,\s*/) : []
	};
};

export default parseFunction;